import { createRouter, createWebHistory } from 'vue-router'
import Home from '../views/Home.vue'
import Login from '../views/Login.vue'
import Profile from '../views/Profile.vue'

const routes = [
  {
    path: '/login',
    name: 'login',
    component: Login,
    meta: {
      title: "Connexion - Groupomania"
    }
  },
  {
    path: '/',
    name: 'home',
    component: Home,
    meta: {
      title: "Accueil - Groupomania"
    }
  },
  {
    path: '/profile',
    name: 'profile',
    component: Profile,
    meta: {
      title: "Profil - Groupomania"
    }
  }
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
})

router.beforeEach((to, from, next) => {
  document.title = to.meta.title
  if (to.fullPath === '/login') {
    if (sessionStorage.getItem('accessToken')) {
      next('/');
    }
  }
  if (to.fullPath === '/' || to.fullPath === '/profile') {
    if (!sessionStorage.getItem('accessToken')) {
      next('/login');
    }
  }
  next();
});

export default router
