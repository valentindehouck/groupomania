import { createStore } from "vuex";
import jwt_decode from "jwt-decode";

const axios = require("axios");

const instance = axios.create({
  baseURL: "http://localhost:3000/api/",
});

export default createStore({
  state: {
    status: "",
    userInfos: {},
    showModal: {
      display: false,
      mode: "",
      post: {
        title: "",
        description: "",
      },
    },
    postsList: [],
  },
  mutations: {
    setStatus: function (state, status) {
      state.status = status;
    },
    setUserInfos: function (state, infos) {
      state.userInfos = infos;
    },
    showModal: function (state, { mode, post }) {
      state.showModal = {
        display: !state.showModal.display,
        mode: mode,
        post: post,
      };
    },
    setAuthorization: function (state, data) {
      const accessToken = data.accessToken;
      instance.defaults.headers.common[
        "Authorization"
      ] = `Bearer ${accessToken}`;
      sessionStorage.setItem("accessToken", accessToken);
      sessionStorage.setItem("refreshToken", data.refreshToken);
      const decoded = jwt_decode(accessToken);
      sessionStorage.setItem("userId", decoded.userId);
    },
    logout: function () {
      sessionStorage.clear();
      window.location.reload();
    },
    setPostsList: function (state, data) {
      state.postsList = data;
    },
    deletePostOnList: function (state, postId) {
      const index = state.postsList.findIndex((post) => post._id === postId);
      state.postsList.splice(index, 1);
    },
    addPostOnList: function (state, postCreated) {
      postCreated.postedBy = {
        _id: state.userInfos.id,
        first_name: state.userInfos.firstName,
        last_name: state.userInfos.lastName,
        avatar: state.userInfos.avatar,
      };
      state.postsList.unshift(postCreated);
    },
    addLikeToPost: function (state, postId) {
      const post = state.postsList.find((post) => post._id === postId);
      const userId = sessionStorage.getItem("userId");
      if (!post.likes.includes(userId)) {
        post.likes.push(userId);
      } else {
        post.likes.splice(userId);
      }
    },
    setNewAvatar: function (state, avatar) {
      state.userInfos.avatar = avatar;
    },
    updateImagePost: function (state, { url, postId }) {
      const post = state.postsList.find((post) => post._id === postId);
      post.imageUrl = url;
    },
  },
  actions: {
    login: ({ commit }, userInfos) => {
      commit("setStatus", "loading");
      return new Promise((resolve, reject) => {
        instance
          .post("auth/login", userInfos)
          .then((response) => {
            commit("setStatus", "");
            commit("setAuthorization", response.data);
            resolve();
          })
          .catch(() => {
            commit("setStatus", "");
            reject();
          });
      });
    },
    createAccount: ({ commit }, userInfos) => {
      commit("setStatus", "loading");
      return new Promise((resolve, reject) => {
        commit;
        instance
          .post("auth/signup", userInfos)
          .then(() => {
            commit("setStatus", "");
            resolve();
          })
          .catch(() => {
            commit("setStatus", "");
            reject();
          });
      });
    },
    getCurrentUser: ({ commit }) => {
      const userId = sessionStorage.getItem("userId");
      commit("setStatus", "loading");
      return new Promise((resolve, reject) => {
        instance
          .get(`profile/${userId}`)
          .then((response) => {
            commit("setStatus", "");
            commit("setUserInfos", response.data);
            resolve();
          })
          .catch(() => {
            commit("setStatus", "");
            reject();
          });
      });
    },
    getPosts: ({ commit }) => {
      commit("setStatus", "loading");
      return new Promise((resolve, reject) => {
        instance
          .get(`posts`)
          .then((response) => {
            commit("setStatus", "");
            commit("setPostsList", response.data);
            resolve();
          })
          .catch(() => {
            commit("setStatus", "");
            reject();
          });
      });
    },
    createPost: ({ commit }, postInfos) => {
      commit("setStatus", "loading");
      return new Promise((resolve, reject) => {
        instance
          .post(`posts/`, postInfos)
          .then((response) => {
            commit("addPostOnList", response.data.newPost);
            commit("setStatus", "");
            resolve();
          })
          .catch(() => {
            commit("setStatus", "");
            reject();
          });
      });
    },
    modifyPost: ({ commit }, { postId, postInfos }) => {
      commit("setStatus", "loading");
      return new Promise((resolve, reject) => {
        instance
          .post(`posts/${postId}`, postInfos)
          .then((response) => {
            commit("setStatus", "");
            if (response.data.newImageUrl) {
              commit("updateImagePost", {
                url: response.data.newImageUrl,
                postId: postId,
              });
            }
            resolve(response.data);
          })
          .catch(() => {
            commit("setStatus", "");
            reject();
          });
      });
    },
    deletePost: ({ commit }, postId) => {
      commit("setStatus", "loading");
      return new Promise((resolve, reject) => {
        instance
          .delete(`posts/${postId}`)
          .then(() => {
            commit("deletePostOnList", postId);
            commit("setStatus", "");
            resolve();
          })
          .catch(() => {
            commit("setStatus", "");
            reject();
          });
      });
    },
    likePost: ({ commit }, postId) => {
      commit("setStatus", "loading");
      return new Promise((resolve, reject) => {
        instance
          .post(`posts/${postId}/like`)
          .then(() => {
            commit("addLikeToPost", postId);
            commit("setStatus", "");
            resolve();
          })
          .catch(() => {
            commit("setStatus", "");
            reject();
          });
      });
    },
    modifyProfile: ({ commit }, profileInfos) => {
      commit("setStatus", "loading");
      const userId = sessionStorage.getItem("userId");
      return new Promise((resolve, reject) => {
        instance
          .post(`profile/${userId}`, profileInfos)
          .then((response) => {
            commit("setStatus", "");
            if (response.data.body.avatar) {
              commit("setNewAvatar", response.data.body.avatar);
            }
            resolve();
          })
          .catch(() => {
            commit("setStatus", "");
            reject();
          });
      });
    },
    deleteUser: ({ commit }, userId) => {
      commit("setStatus", "loading");
      return new Promise((resolve, reject) => {
        instance
          .delete(`profile/${userId}`)
          .then(() => {
            commit("setStatus", "");
            commit("logout");
            resolve();
          })
          .catch(() => {
            commit("setStatus", "");
            reject();
          });
      });
    },
  },
  getters: {
    getUserInfos: (state) => {
      return state.userInfos;
    },
    showModal: (state) => {
      return state.showModal;
    },
    getStatus: (state) => {
      return state.status;
    },
    getPosts: (state) => {
      return state.postsList;
    },
  },
  modules: {},
});

instance.interceptors.request.use(
  (instance.defaults.headers.common[
    "Authorization"
  ] = `Bearer ${sessionStorage.getItem("accessToken")}`)
);

instance.interceptors.response.use(
  (response) => {
    return response;
  },
  async (error) => {
    const originalRequest = error.config;
    if (
      error.config.url != "auth/refreshToken" &&
      !originalRequest._retry &&
      error.response.status === 401
    ) {
      originalRequest._retry = true;
      const refreshToken = sessionStorage.getItem("refreshToken");
      try {
        const request = await instance.post("auth/refreshToken", {
          token: refreshToken,
        });
        const { accessToken } = request.data;
        originalRequest.headers["Authorization"] = `Bearer ${accessToken}`
        sessionStorage.setItem("accessToken", accessToken);
        return instance(originalRequest)
      } catch {
        return;
      }
    }
    sessionStorage.clear()
  }
);
