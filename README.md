# Groupomania - Créer un réseau social d’entreprise


## Lancement du projet en local

* 📋 Faire un git clone pour récupérer le projet depuis le repo GitLab
* 🗃️ Pour la base de données, il vous suffit de extraire le fichier .ZIP transmis, d'avoir Mongo en local et d'éxecuter `mongorestore -d NOM_DE_DB_SOUHAITEE chemin/vers/le/dossier/extrait/groupomania`
* 🛠️ Effectuer un `npm i` dans le dossier frontend et backend
* 🛠️ Créer le .env dans le dossier backend à partir du modèle et y renseigner l'url de la DB Mongo en local (crée précédemment), ainsi que le secret pour l'accessToken et rehreshToken
* 👨‍💻 Dans le dossier frontend, lancer la commande : `npm run serve`
* 👨‍💻 Dans le dossier backend, lancer la commande : `npm start`
* 🎉 Félicitations, tout est configuré, vous pouvez accéder à l'interface via l'adresse : http://localhost:8080

## Présentation

La direction a promis des mesures d’amélioration de la communication entre collègues, notamment la mise en place d’un nouvel outil numérique. Il s’agit de la création d’un réseau social interne moderne, qui permettra aux employés de se connaître dans un cadre plus informel.

Suite à l’enquête, le comité de pilotage a étudié les réponses des salariés aux différents questionnaires, ceci ayant permis la mise en place d’une liste de fonctionnalités minimales que devrait contenir le réseau social.

## Attendus

**Aspect graphique :**

Respecter l’identité graphique fournie dans le brief

Produire quelque chose de responsive qui s'adapte aux desktop, tablette et mobile

**Côté technique :**

Tu vas donc devoir mettre en place le backend, le frontend et la base de données

Le projet doit être codé en JavaScript et respecter les standards WCAG

Il est obligatoire d’utiliser un framework front-end JavaScript. Comme on part de zéro, libre à toi d’utiliser celui que tu préfères (React, Vue, Angular…)

Pour la base de données, tu peux utiliser les outils de ton choix. Tu peux utiliser soit une base de données non relationnelle, comme mongoDB par exemple, soit une base de données relationnelle

Pense à bien fournir un README avec ton code, expliquant comment installer le site sur un nouveau poste.

## Choix techniques pour ce projet

Front-end en VueJS

Back-end en ExpressJS (NodeJS)

Base de donées avec MongoDB

## Spécifications fonctionnelles

**Page de connexion**
Une page de connexion permettant à l’utilisateur de se connecter, ou bien de créer un compte s’il n’en possède pas. Ici il faut demander le minimum d’informations, la connexion doit se faire à partir de deux éléments : le mail de l’employé, et un mot de passe.
Détails de la fonctionnalité de connexion :
* Un utilisateur doit avoir la possibilité de se déconnecter.
* La session de l’utilisateur persiste pendant qu’il est connecté.
* Les données de connexion doivent être sécurisées.

**Page d’accueil**
La page d’accueil doit lister les posts créés par les différents utilisateurs. On voudra que les posts soient listés de façon antéchronologique (du plus récent au plus ancien).

**Création d’un post**
* Un utilisateur doit pouvoir créer un post.
* Un post doit pouvoir contenir du texte et une image.
* Un utilisateur doit aussi pouvoir modifier et supprimer ses posts

**Système de like**
Un utilisateur doit pouvoir liker un post, une seule fois pour chaque post.

**Rôle administrateur**
Dans le but de pouvoir faire de la modération si nécessaire, il faudra créer un utilisateur “administrateur”. Celui-ci aura les droits de modification / suppression sur tous les posts du réseau social.

## Identité graphique

Police d’écriture : Lato.

Couleurs :
* Primaire : #FD2D01
* Secondaire : #FFD7D7
* Tertiaire : #4E5166

## Modèles des données

**users** :
{
    _id: ObjectId,
    first_name: String,
    last_name: String,
    email: String,
    password: String,
    avatar: String,
    admin: Boolean (default false)
}

**Post** :
{
    _id: ObjectId,
    title: String,
    description: String,
    imageUrl: String,
    likes: [ String ]
    postedBy: String
    createdAt: Date,
    updatedAt: Date
}

**Routes API :**

Méthode   | Route             |  Body          | Typeof réponse |  Fonction |
----------| ---------------   | -------------- | ---------------| ----------|
POST      | /api/auth/signup  | {email, fist_name, last_name, password} | {message:string}      | Hachage du mot de passe de l'utilisateur, ajout de l'utilisateur à la base de données |
POST      | /api/auth/login   | {email,password} | {accessToken, refreshToken}| Verification des infos d'authentification, renvoie les deux token |
POST      | /api/auth/refreshToken   | _ | { accessToken }| Génération d'un nouvel accessToken à partir du refreshToken |
GET       | /api/profile/:id  | _ | {first_name, last_name, avatar, admin}| Renvoie les informations sur l'utilisateur actuel |
POST      | /api/profile/:id  | {avatar, password, email, first_name, last_name} | {first_name, last_name, avatar, email } |  Modifie les informations de l'utilisateur actuel |
DELETE    | /api/profile/:id  | _ | _ | Supprime le compte d'un utilisateur |
GET       | /api/posts/       | _ | Array of posts| Renvoie un tableau contenant l'ensemble des posts |
POST      | /api/post/  | {title, description, image} | _ |  Crétation d'un post |
POST      | /api/post/:id  | {image, title, description} | _ |  Modifie un post |
POST      | /api/post/:id/like  | _ | _ |  Ajout/Supprime un like sur un post |
DELETE    | /api/post/:id  | _ | _ | Supprime un post |