const User = require("../models/user");
const Post = require("../models/post");
const fs = require("fs");
const bcrypt = require("bcrypt");

exports.getCurrentUser = async (req, res, next) => {
  try {
    const user = await User.findOne({ _id: req.params.id });
    if (!user) {
      return res.sendStatus(404);
    }
    res.status(200).json({
      firstName: user.first_name,
      lastName: user.last_name,
      email: user.email,
      id: user._id,
      avatar: user.avatar,
      admin: user.admin,
    });
  } catch {
    res.sendStatus(500);
  }
};

exports.updateProfile = async (req, res, next) => {
  try {
    const userId = req.params.id;
    if (req.user.userId != userId) {
      res.sendStatus(401);
      return;
    }
    const body = req.body;
    const user = await User.findOne({ _id: userId });
    if (req.file != undefined) {
      const filename = user.avatar.split("/images/profile/")[1];
      if (filename != "avatar-defaut.jpg") {
        fs.unlink(`images/profile/${filename}`, async (err) => {
          if (err) console.log(err);
        });
        body.avatar = `${req.protocol}://${req.get("host")}/images/profile/${
          req.file.filename
        }`;
      } else {
        body.avatar = `${req.protocol}://${req.get("host")}/images/profile/${
          req.file.filename
        }`;
      }
    } else {
      delete body.file;
    }
    if (body.password != "") {
      const validPwd = await bcrypt.compare(
        req.body.actualPassword,
        user.password
      );
      if (!validPwd) {
        return res.sendStatus(400);
      }
      const passwordHash = await bcrypt.hash(body.password, 10);
      body.password = passwordHash;
      delete body.actualPassword;
    } else {
      delete body.password;
      delete body.actualPassword;
    }
    await User.updateOne({ _id: req.params.id }, body, {
      runValidators: true,
      context: "query",
    });
    res.status(200).json({ body });
  } catch {
    res.sendStatus(500);
  }
};

exports.deleteProfile = async (req, res, next) => {
  try {
    const userId = req.params.id;
    if (req.user.userId != userId) {
      res.sendStatus(401);
      return;
    }
    const user = await User.findOne({ _id: userId });
    const avatar = user.avatar.split("/images/profile/")[1];
    const posts = await Post.find({ postedBy: userId });
    posts.forEach((post) => {
      if (post.imageUrl) {
        const filename = post.imageUrl.split("/images/")[1];
        fs.unlink(`images/${filename}`, async (err) => {
          if (err) console.error(err);
          else {
            await Post.deleteOne({ _id: post.id });
          }
        });
      }
    });
    if (avatar != "avatar-defaut.jpg") {
      fs.unlink(`images/profile/${avatar}`, async (err) => {
        if (err) console.error(err);
      });
    }
    await User.deleteOne({ _id: userId });
    res.sendStatus(200);
  } catch {
    res.sendStatus(400);
  }
};
