const Post = require("../models/post");
const fs = require("fs");
const User = require("../models/user");

exports.getAll = async (req, res, next) => {
  try {
    const allPosts = await Post.find()
      .populate("postedBy", "first_name last_name avatar")
      .sort({ createdAt: -1 });
    res.status(200).json(allPosts);
  } catch {
    res.sendStatus(400);
  }
};

exports.createPost = (req, res, next) => {
  try {
    const newPost = new Post({
      title: req.body.title,
      description: req.body.description,
      likes: [],
      postedBy: req.user.userId,
    });

    if (req.file != undefined) {
      newPost.imageUrl = `${req.protocol}://${req.get("host")}/images/${
        req.file.filename
      }`;
    } else {
      newPost.imageUrl = null;
    }

    newPost.save();
    res.status(201).json({ newPost });
  } catch {
    res.sendStatus(400);
  }
};

exports.modifyPost = async (req, res, next) => {
  try {
    const post = await Post.findOne({ _id: req.params.id });
    const user = await User.findOne({ _id: req.user.userId })
    if (req.user.userId != post.postedBy && !user.admin) {
      res.sendStatus(401);
      return;
    }
    if (req.file != undefined) {
      const url = `${req.protocol}://${req.get("host")}/images/${
        req.file.filename
      }`;
      if (post.imageUrl != null) {
        const filename = post.imageUrl.split("/images/")[1];
        fs.unlink(`images/${filename}`, async (err) => {
          if (err) console.log(err);
          else {
            await Post.updateOne(
              { _id: req.params.id },
              {
                ...req.body,
                imageUrl: url,
              }
            );
          }
        });
      } else {
        await Post.updateOne(
          { _id: req.params.id },
          {
            ...req.body,
            imageUrl: url,
          }
        );
      }
      res.status(200).json({newImageUrl: url});
    } else {
      await Post.updateOne({ _id: req.params.id }, { ...req.body });
      res.sendStatus(200);
    }
  } catch {
    res.sendStatus(400);
  }
};

exports.deletePost = async (req, res, next) => {
  try {
    const post = await Post.findOne({ _id: req.params.id });
    const user = await User.findOne({ _id: req.user.userId })
    if (req.user.userId != post.postedBy && !user.admin) {
      res.sendStatus(401);
      return;
    }
    if (post.imageUrl != null) {
      const filename = post.imageUrl.split("/images/")[1];
      fs.unlink(`images/${filename}`, async () => {
        await Post.deleteOne({ _id: req.params.id });
      });
    } else {
      await Post.deleteOne({ _id: req.params.id });
    }
    res.sendStatus(200);
  } catch {
    res.sendStatus(500);
  }
};

exports.like = async (req, res, next) => {
  const userId = req.user.userId;
  try {
    const post = await Post.findOne({ _id: req.params.id });
    if (post.likes.includes(userId)) {
      await Post.updateOne(
        { _id: req.params.id },
        {
          $pull: { likes: userId },
        }
      );
      res.sendStatus(200);
    } else {
      await Post.updateOne(
        { _id: req.params.id },
        {
          $push: { likes: userId },
        }
      );
      res.sendStatus(200);
    }
  } catch {
    res.sendStatus(404);
  }
};
