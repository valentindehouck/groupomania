const bcrypt = require("bcrypt");
const User = require("../models/user");
const jwt = require("jsonwebtoken");

exports.signup = async (req, res, next) => {
  try {
    const passwordHash = await bcrypt.hash(req.body.password, 10);

    const user = new User({
      first_name: req.body.firstName,
      last_name: req.body.lastName,
      email: req.body.email,
      password: passwordHash,
      avatar: "http://localhost:3000/images/profile/avatar-defaut.jpg",
      admin: false,
    });
    user.save();

    res.sendStatus(201);
  } catch {
    res.sendStatus(400);
  }
};

exports.login = async (req, res, next) => {
  try {
    const user = await User.findOne({ email: req.body.email });
    if (!user) {
      return res.sendStatus(404);
    }

    const validPwd = await bcrypt.compare(req.body.password, user.password);

    if (!validPwd) {
      return res.sendStatus(400);
    }
    const accessToken = generateAccessToken(user._id);
    const refreshToken = generateRefreshToken(user._id);
    res.status(200).json({ accessToken, refreshToken });
  } catch {
    res.sendStatus(400);
  }
};

exports.refreshToken = async (req, res) => {
  const token = req.body.token
  try {
    const decoded = jwt.verify(token, process.env.SECRET_REFRESH);
    const refreshedToken = generateAccessToken(decoded.userId);
    res.send({
      accessToken: refreshedToken,
    });
  } catch {
    return res.sendStatus(403);
  }
};

function generateAccessToken(userId) {
  return jwt.sign({ userId }, process.env.SECRET_TOKEN, {
    expiresIn: "3600s",
  });
}

function generateRefreshToken(userId) {
  return jwt.sign({ userId }, process.env.SECRET_REFRESH, {
    expiresIn: "24h",
  });
}
