const express = require('express');
const router = express.Router();

const profile = require('../controllers/profile');
const authMiddleware = require('../middleware/verifyToken')
const multer = require('../middleware/multerUser')

router.get('/:id', authMiddleware, profile.getCurrentUser)
router.post('/:id', authMiddleware, multer, profile.updateProfile)
router.delete('/:id', authMiddleware, profile.deleteProfile)

module.exports = router;