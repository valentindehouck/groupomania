const express = require('express');
const router = express.Router();

const posts = require('../controllers/posts');
const authMiddleware = require('../middleware/verifyToken')
const multer = require('../middleware/multerPost')


router.get('/', authMiddleware, posts.getAll)
router.post('/', authMiddleware, multer, posts.createPost)
router.post('/:id', authMiddleware, multer, posts.modifyPost)
router.delete('/:id', authMiddleware, posts.deletePost)
router.post('/:id/like', authMiddleware, posts.like)

module.exports = router;