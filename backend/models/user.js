const mongoose = require("mongoose");
const uniqueValidator = require("mongoose-unique-validator");

const userModel = mongoose.Schema({
  first_name: { type: String, required: true },
  last_name: { type: String, required: true },
  avatar: { type: String, required: false },
  email: { type: String, required: true, unique: true, uniqueCaseInsensitive: true },
  password: { type: String, required: true },
  admin: { type: Boolean, required: true }
});

userModel.plugin(uniqueValidator);

module.exports = mongoose.model("User", userModel);
