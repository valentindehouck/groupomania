const multer = require('multer');
const path = require('path');

const storage = multer.diskStorage({
    destination: (req, file, callback) => {
      callback(null, 'images/profile');
    },
    
    filename: (req, file, callback) => {
      callback(null, req.user.userId + path.parse(file.originalname).ext);
    },
  });

module.exports = multer({ storage }).single('file');
