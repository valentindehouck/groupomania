const multer = require('multer');
const path = require('path');

const storage = multer.diskStorage({
  destination: (req, file, callback) => {
    callback(null, 'images');
  },
  
  filename: (req, file, callback) => {
    const name = file.originalname.split(' ').join('_');
    callback(null, path.parse(name).name + Date.now() + path.parse(name).ext);
  },
});

module.exports = multer({ storage }).single('file');
