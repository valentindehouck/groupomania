const express = require("express");

const app = express();
const mongoose = require("mongoose");
const path = require("path");
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

require("dotenv").config();

mongoose
  .connect(process.env.MONGODB_URL, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  })
  .then(() => console.log("Connexion à MongoDB réussie !"))
  .catch(() => console.log("Connexion à MongoDB échouée !"));

app.use((req, res, next) => {
  res.setHeader("Access-Control-Allow-Origin", "*");
  res.setHeader(
    "Access-Control-Allow-Headers",
    "Origin, X-Requested-With, Content, Accept, Content-Type, Authorization"
  );
  res.setHeader(
    "Access-Control-Allow-Methods",
    "GET, POST, PUT, DELETE, PATCH, OPTIONS"
  );
  next();
});

const authRoutes = require("./routes/auth.js");
const profileRoutes = require("./routes/profile.js");
const postsRoutes = require("./routes/posts.js")

app.use("/images", express.static(path.join(__dirname, 'images')));

app.use("/api/auth", authRoutes);
app.use("/api/profile", profileRoutes);
app.use("/api/posts", postsRoutes);

module.exports = app;